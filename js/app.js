$('document').ready(function(){
	var prevCity = Cookies.get(PREF_CITY);
	if (prevCity !== undefined) {
		var url = BASE_API + prevCity + API_ADDITIONS;
		getWeatherData(url);
	}
});

$('#locationButton').click(function(){ 
	$('.weekDays').css("display", "none");
	document.getElementById("loader").style.display = "block";
	var cityName = $('#locationInput').val();
	
	$('#locationInput').val('');
	var url = BASE_API + cityName + API_ADDITIONS;
	getWeatherData(url);
});

$('#locationInput').keypress(function(e) {
    if(e.which == 13) {
        $('#locationButton').click();
    }
});

n =  new Date();
y = n.getFullYear();
m = n.getMonth() + 1;
d = n.getDate();
document.getElementById("date").innerHTML = m + "/" + d + "/" + y;

function getWeatherData(url) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if(xhr.readyState === 4) {
			const cityWeatherInfo = JSON.parse(xhr.responseText);
			const city = cityWeatherInfo.city;
			const dataList = cityWeatherInfo.list;

			if (city === undefined) {
				showNoCityAlert();
				return;
			}	

			//cookies
			const cityName = city.name;
			Cookies.set(PREF_CITY, cityName, { expires: 7 });
			$('#city-name').text(cityName);
			

			var today = new Day();
			var currentDay = dataList[0];

			today.currentTemperature = currentDay.main.temp;
			today.minTemp = currentDay.main.temp_min;
			today.maxTemp = currentDay.main.temp_max;
			today.windSpeed = currentDay.wind.speed;
			today.windDeg = currentDay.wind.deg;
			today.weatherType = currentDay.weather[0].main;

			todayHTML(today);

			var date = new Date();
			var currentHour = date.getHours();
			var multOfThree = currentHour - currentHour % 3;
			var timeRemaining = 8 - multOfThree / 3;

			for (var i = timeRemaining; i < dataList.length; i += 8) {
				var nextDay = dataList[i];
				var tomorrow = new Day();

				tomorrow.maxTemp = nextDay.main.temp_max;
				tomorrow.minTemp = nextDay.main.temp_min;
				tomorrow.weatherType = nextDay.weather[0].main;

				for (var j = i; j < i + 8, j < dataList.length; j++) {
					var allTemps = dataList[j];
					var tempMax = allTemps.main.temp_max;
					var tempMin = allTemps.main.temp_min;

					if (tempMax > tomorrow.maxTemp) {
						tomorrow.maxTemp = tempMax;
					} 

					if (tempMin < tomorrow.minT) {
						tomorrow.minTemp = tempMin;
					}
				}

				var dayPos = (8 + i - timeRemaining) / 8 - 1;

				setupDay(tomorrow, dayPos);
				
			}
			nameDays();
			document.getElementById("loader").style.display = "none";
			$('.weekDays').css("display", "flex");


			}
		};
		xhr.open('GET', url);
		xhr.send();
	}


	function showNoCityAlert() {
		document.getElementById("loader").style.display = "none";
		alert('Please enter a valid city.');
	}

	function todayHTML(today) {
		var currentTempInt = Math.round(today.currentTemperature); 
		$('#today-temp').text(currentTempInt + String.fromCharCode(176) + "C");

		var windSpeed = Math.round(today.minTemp);
		$('#currentWindSpeed').text(windSpeed + " km/h");

		$('#currentDeg').text(getCardinal(today.windDeg));


		$('#today-main-img').attr("src", "images/" + iconForDescription(today.weatherType));

	}

//given "0-360" returns the nearest cardinal direction "N/NE/E/SE/S/SW/W/NW/N" 
function getCardinal(angle) {
        //easy to customize by changing the number of directions you have 
        var directions = 8;
        
        var degree = 360 / directions;
        angle = angle + degree/2;
        
        if (angle >= 0 * degree && angle < 1 * degree)
        	return "North";
        if (angle >= 1 * degree && angle < 2 * degree)
        	return "North East";
        if (angle >= 2 * degree && angle < 3 * degree)
        	return "East";
        if (angle >= 3 * degree && angle < 4 * degree)
        	return "South East";
        if (angle >= 4 * degree && angle < 5 * degree)
        	return "South";
        if (angle >= 5 * degree && angle < 6 * degree)
        	return "South West";
        if (angle >= 6 * degree && angle < 7 * degree)
        	return "West";
        if (angle >= 7 * degree && angle < 8 * degree)
        	return "North West";
        //Should never happen: 
        return "North";
    }

    function iconForDescription(description) {

    	var text = "";

    	switch(description) {
    		case "Rain":
    		text = "icon-10.svg";
    		break;
    		case "Snow":
    		text = "icon-14.svg";
    		break;
    		case "Clear":
    		text = "icon-2.svg";
    		break;
    		case "Cloudy":
    		text = "icon-6.svg";
    		break;
    		default:
    		text = "icon-1.svg";
    	}

    	return text;
    }

function setupDayForElements(img, dtemp, ntemp, d) {
   	var maxTempInt = Math.round(d.maxTemp); 
    dtemp.textContent = (maxTempInt + String.fromCharCode(176) + "C");

    var minTempInt = Math.round(d.minTemp);
    ntemp.textContent = (minTempInt + String.fromCharCode(176));

    img.src = ("images/" + iconForDescription(d.weatherType));
} //takes the data from d(Day object ---- see todayHTML) and stores them into the img, dtemp, ntemp parameters

function setupDay(dayData, index) {
	var parentDiv = document.getElementById("weatherParentDiv");
	var childNode = parentDiv.children[index];
	var secondChildNode = childNode.children[1];

	var imgChildNode = secondChildNode.children[0];
	var dayTemp = secondChildNode.children[1];
	var nightTemp = secondChildNode.children[2];

	setupDayForElements(imgChildNode, dayTemp, nightTemp, dayData);

} //selects DOM elements sends them to setupDayForElements function



function nameDays() {

	var todaysDate = new Date();
	var todaysDateIndex = todaysDate.getDay();
	var days = new Array(7);
	days[0] = "Sunday";
	days[1] = "Monday";
	days[2] = "Tuesday";
	days[3] = "Wednesday";
	days[4] = "Thursday";
	days[5] = "Friday";
	days[6] = "Saturday";
	var n = days[todaysDateIndex];


	for (var i = 2; i <= 6; i++) {
		if (todaysDateIndex + i >= days.length) {
			var nextDay = todaysDateIndex + i - days.length;
		} else {
			var nextDay = todaysDateIndex + i;
		}
		var dayName = days[nextDay];
		setupDayNameForHeadElements(i, dayName);
	}
}

function setupDayNameForHeadElements(index, dayName) {
	var parentDiv = document.getElementById("weatherParentDiv");
	var childNode = parentDiv.children[index - 1];
	var theadChildNode = childNode.children[0]; 

	theadChildNode.textContent = dayName;

}
